# OBS Random Source

From a list of inactive sources, selects randomly one of them and toggles them for x seconds.

Based on [obs-random-text](https://github.com/revenkroz/obs-random-text)